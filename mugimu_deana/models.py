from django.db import models
from polymorphic.models import PolymorphicModel
from datetime import datetime
from account import models as amod
# Create your models here.

class Category(models.Model):
    # id
    codename = models.TextField(blank=True, null=True, unique=True)
    name = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class Product(PolymorphicModel):
    # id
    name = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category')
    price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True) #999,999.99
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    brand = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

class BulkProduct(Product):
    # id
    serial_number = models.TextField(blank=True)
    quantity = models.IntegerField(default=0)
    reorder_trigger = models.IntegerField(default=0)
    reorder_quantity = models.IntegerField(default=0)

class UniqueProduct(Product):
    #id
    serial_number = models.TextField(blank=True, null=True)

class RentalProduct(Product):
    #id
    serial_number = models.TextField(blank=True, null=True)
    quantity = models.IntegerField(default=0)

class ProductImage(models.Model):
    product = models.ForeignKey('catalog.Product', related_name='pictures', null=True)
    imgurl = models.TextField(blank=True, null=True)

class Sale(models.Model):
    sale_date = models.DateTimeField(auto_now_add=True)
    subtotal = models.DecimalField()
    total = models.DecimalField()
    is_fully_paid = models.BooleanField(default=False)
    user = models.ForeignKey(amod.FomoUser, on_delete=models.CASCADE)
    ship_address = models.TextField()
    city = models.TextField()
    state = models.TextField()


    def record_sale(User, Ship_address, City, State, PaymentAmount):
        items = Kart.objects.filter(user = User, is_active = True)
        sale = sale()
        sale.user = User
        sale.city = City
        sale.state = State
        sale.ship_address = Ship_address
        subtotal = 0
        for i in items:
            product = i.product
            SI = SaleItem()
            SI.product = i.product
            SI.quantity = i.quantity
            salePrice = product.price * SI.quantity
            SI.sale = i.sale
            subtotal = salePrice + subtotal
            if product.serial_number:
                product.delete()
            else:
                product.quantity = product.quantity - SI.quantity
                product.save()
            SI.save()
        sale.subtotal = subtotal
        sale.total = subtotal*1.0725
        p = payment()
        p.amount = PaymentAmount
        p.sale = sale
        if p.amount >= sale.total:
            sale.is_fully_paid = True
        p.save()
        sale.save()

class SaleItem(models.Model):
    sale_price = models.DecimalField()
    quantity = models.IntegerField(default=1)
    product = models.ForeignKey(Product)
    sale = models.ForeignKey(Sale)

class Payment(models.Model):
    amount = models.DecimalField()
    sale = models.ForeignKey(sale, on_delete=models.CASCADE)

class Kart(models.Model):
    quantity = models.IntegerField()
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    user = models.ForeignKey(amod.FomoUser, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
