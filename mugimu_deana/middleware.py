def Last5ProductsMiddleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        print('>>>>>>> middleware called at beginning')

        request.last5 = request.session.get['last5']
        #Get the last 5 from request.session
        Last5_list = request.session.get('last5')
        if last5_list is None or isinstance(request.last5, list):
            last5_list = []
        request.last5 = last5_list

        #let django continue
        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        request.session['last5'] = request.last5[:5]
        print('>>>>>>> middleware called at END')
        return response

    return middleware
