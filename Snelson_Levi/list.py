from django.conf import settings
from django_mako_plus import view_function
from catalog import models as cmod
from django.http import HttpResponseRedirect, HttpResponse

from .. import dmp_render, dmp_render_to_string



@view_function
def process_request(request):

    products = cmod.Product.objects.order_by('name').all()

    context = {
        'products': products,
    }

    return dmp_render(request, 'list.html', context)

@view_function
def get_quantity(request):
    try:
        product = cmod.BulkProduct.objects.get(id=request.urlparams[0])
    except (TypeError, cmod.BulkProduct.DoesNotExist):
        return HttpResponseRedirect('/catalog/list/')

    return HttpResponse(product.quantity)
