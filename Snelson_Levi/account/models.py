from django.db import models
from django.contrib.auth.models import AbstractUser
from datetime import datetime

# Define models here

class FomoUser(AbstractUser):
    #username - AbstractUser
    #password - AbstractBaseUser
    #first_name - AbstractUser
    #last_name - AbstractUser
    #email - AbstractUser
    #date_joined - AbstractUser
    #last_login - AbstractBaseUser
    #objects = AbstractUser()
    birthdate = models.DateTimeField('birthdate of user')
    gender = models.TextField()


# class Question(models.Model):
#     question_text = models.CharField(max_length=200)
#     pub_date = models.DateTimeField('date published')
#
#
# class Choice(models.Model):
#     question = models.ForeignKey(Question, on_delete=models.CASCADE)
#     choice_text = models.CharField(max_length=200)
#     votes = models.IntegerField(default=0)
