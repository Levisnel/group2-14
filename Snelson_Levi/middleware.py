def Last5ProductsMiddleware(get_response):
    # One-time configuration and initialization.

    def middleware(request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
    
        request.last5 = request.session.get('last5products')
        if request.last5 is None:
            request.last5 = []

        #Django will call your view function here
        response = get_response(request)

        # Code to be executed for each request/response after
        # the view is called.
        request.session['last5products'] = request.last5[:5]
        return response

    return middleware
