# #!/usr/bin/env python3
#
# from django.core import management
# from django.db import connection
# from datetime import datetime
# import os, os.path, sys
#
#
# # ensure the user really wants to do this
# areyousure = input('''
#   You are about to drop and recreate the entire database.
#   All data are about to be deleted.  Use of this script
#   may cause itching, vertigo, dizziness, tingling in
#   extremities, loss of balance or coordination, slurred
#   speech, temporary zoobie syndrome, longer lines at the
#   testing center, changed passwords in Learning Suite, or
#   uncertainty about whether to call your professor
#   'Brother' or 'Doctor'.
#
#   Please type 'yes' to confirm the data destruction: ''')
# if areyousure.lower() != 'yes':
#     print()
#     print('  Wise choice.')
#     sys.exit(1)
#
# # initialize the django environment
# os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
# import django
# django.setup()
#
#
# # drop and recreate the database tables
# print()
# print('Living on the edge!  Dropping the current database tables.')
# with connection.cursor() as cursor:
#     cursor.execute("DROP SCHEMA public CASCADE")
#     cursor.execute("CREATE SCHEMA public")
#     cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
#     cursor.execute("GRANT ALL ON SCHEMA public TO public")
#
# # make the migrations and migrate
# management.call_command('makemigrations')
# management.call_command('migrate')
#
#
# # imports for our project
# from account.models import FomoUser
#
# # User object
# # make 3
# u1 = FomoUser()
# u1.username = 'john_smith'
# u1.first_name = 'John'
# u1.last_name = 'Smith'
# u1.birthdate = '2006-10-25 14:30:59'
# u1.email = 'john@email.com'
# u1.set_password('password1')
# u1.gender = 'male'
# u1.save()
#
# u2 = FomoUser()
# u2.username = 'rey_kenobi'
# u2.first_name = 'Rey'
# u2.last_name = 'Kenobi'
# u2.birthdate = '1991-8-1 14:30:59'
# u2.email = 'rey@email.com'
# u2.set_password('password2')
# u2.gender = 'female'
# u2.save()
#
# u3 = FomoUser()
# u3.username = 'darth_bane'
# u3.first_name = 'Darth'
# u3.last_name = 'Bane'
# u3.birthdate = '1970-10-4 12:15:30'
# u3.email = 'darth@email.com'
# u3.set_password('password3')
# u3.gender = 'male'
# u3.save()

#Select data from users created
import os, os.path, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()
from account.models import FomoUser

s1 = FomoUser.objects.get(id=3)
print (s1.email)

s2 = FomoUser.objects.get(first_name='Rey')
print (s2.first_name)
