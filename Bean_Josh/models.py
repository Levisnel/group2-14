# Account APP
from django.db import models
from django.contrib.auth.models import AbstractUser

#This is my user class
class FomoUser(AbstractUser):
    #Inheriting from AbstractBaseUser
        #password
        #last_login
    #Inheriting from AbstractUser
        #username
        #first_name
        #last_name
        #email
        #date_joined
     birthdate = models.DateTimeField('birthdate of user')
     gender = models.TextField()
     address = models.TextField()
     city = models.TextField()
     state = models.TextField()
     postal = models.TextField()
     phone = models.TextField()
