from django.core import management
from django.db import connection
from datetime import datetime
import os, os.path, sys

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()


# drop and recreate the database tables
with connection.cursor() as cursor:
     cursor.execute("DROP SCHEMA public CASCADE")
     cursor.execute("CREATE SCHEMA public")
     cursor.execute("GRANT ALL ON SCHEMA public TO postgres")
     cursor.execute("GRANT ALL ON SCHEMA public TO public")

# make the migrations and migrate
management.call_command('makemigrations')
management.call_command('migrate')


# Creation of users for dummy data
from account.models import FomoUser

# User object
u1 = FomoUser()
u1.username = 'bmack'
u1.first_name = 'MacKenzie'
u1.last_name = 'Bowman'
u1.birthdate = '1994-11-02 7:32:34'
u1.email = 'bowman@email.com'
u1.set_password('password')
u1.phone = "242-212-1234"
u1.address = '123 Center Street'
u1.city = 'Provo'
u1.state = 'UT'
u1.postal = '84606'
u1.gender = 'female'
u1.save()

# User object
u2 = FomoUser()
u2.username = 'bean22'
u2.first_name = 'Josh'
u2.last_name = 'Bean'
u2.birthdate = '1992-05-28 16:10:29'
u2.email = 'bean@email.com'
u2.set_password('password1')
u2.phone = "222-232-5434"
u2.address = '12345 Center Avenue'
u2.city = 'Provo'
u2.state = 'UT'
u2.postal = '84606'
u2.gender = 'male'
u2.save()

u3 = FomoUser()
u3.username = 'alfie55'
u3.first_name = 'Alfredo'
u3.last_name = 'Olsen'
u3.birthdate = '1993-07-02 03:12:10'
u3.email = 'olsen@email.com'
u3.set_password('password2')
u3.phone = "555-222-5444"
u3.address = '123456 Center Circle'
u3.city = 'Provo'
u3.state = 'UT'
u3.postal = '84606'
u3.gender = 'male'
u3.save()

u4 = FomoUser()
u4.username = 'dena'
u4.first_name = 'Deana'
u4.last_name = 'Mugimu'
u4.birthdate = '1993-10-17 04:52:50'
u4.email = 'mugimu@email.com'
u4.set_password('password3')
u4.phone = "234-754-0000"
u4.address = '0987 Center Lane'
u4.city = 'Provo'
u4.state = 'UT'
u4.postal = '84606'
u4.gender = 'female'
u4.save()

u5 = FomoUser()
u5.username = 'Mr. Snel'
u5.first_name = 'Levi'
u5.last_name = 'Snelson'
u5.birthdate = '1992-01-22 16:33:10'
u5.email = 'snelson@email.com'
u5.set_password('password4')
u5.phone = "000-111-2222"
u5.address = '67548 Center Way'
u5.city = 'Provo'
u5.state = 'UT'
u5.postal = '84606'
u5.gender = 'male'
u5.save()
